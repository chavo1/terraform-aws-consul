## This repo contains Terraform - AWS - Consul module which deploys Consul cluster in AWS over HTTP.

### How to use it - please refer to the [example](https://bitbucket.org/chavo1/terraform-aws-consul/src/master/example/).

### Prepare you own AWS AMI with [Packer](https://www.packer.io/)
- [PACKER-CONSUL-SERVER](https://github.com/chavo1/packer-consul-server)
